﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationTriggerMultiple: MonoBehaviour {

	[Header("Scene9")]
	public UnityEvent [] events;

	Animator anim;

	void Start()
	{
		anim=GetComponent<Animator>();
	}

	public void animate(int eventPoz)
	{
		events[eventPoz].Invoke();
	}
}
