﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class CleanableElement : MonoBehaviour, IDragHandler {

	private Image img;
	private Texture2D initialTex;
	public float brushSize = 100f;
	public float decay = 0.2f;


	void Start()
	{
		img = GetComponent<Image>();
		initialTex = img.sprite.texture;

	}

	void OnDisable()
	{
		
		Reset();
	}
		


	public void OnDrag(PointerEventData eventData)
	{

		try{
		RectTransform rect1 = GetComponent<RectTransform>();
		Vector2 localCursor;
		Vector2 pos1 = eventData.position;
		if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(rect1, pos1, eventData.pressEventCamera, out localCursor))
			return;
	
		Vector2 uv = new Vector2(localCursor.x/rect1.sizeDelta.x,localCursor.y/rect1.sizeDelta.y);
		Vector2 pixelCoords = new Vector2(Mathf.Floor(uv.x * img.sprite.texture.width), Mathf.Floor(uv.y * img.sprite.texture.height));

		Texture2D tex = img.sprite.texture;
		Texture2D workTex = new Texture2D(tex.width, tex.height, TextureFormat.ARGB32, false);

		Color c = tex.GetPixel((int)pixelCoords.x, (int)pixelCoords.y);

		for(int i=0; i< tex.width; i++)
			for (int j=0; j<tex.height; j++)
			{
				workTex.SetPixel(i, j, tex.GetPixel(i, j));

				if ((i > pixelCoords.x - brushSize) && (i < pixelCoords.x + brushSize))
					if (j > pixelCoords.y - brushSize && j < pixelCoords.y + brushSize) 
					{
					
						float coef = (pixelCoords.x - brushSize + pixelCoords.y - brushSize)/(brushSize*2);
						Color pixel = workTex.GetPixel(i, j); 
						pixel.a = Mathf.Max(pixel.a - decay * coef, 0f);
						workTex.SetPixel(i,j, pixel);
					}

			}

	
		
		workTex.Apply();
		Sprite sp = Sprite.Create(workTex, new Rect(0, 0, workTex.width, workTex.height), new Vector2(0, 0));
		img.sprite = sp;
		}
		catch(Exception ex)
		{
		}




	}

	private void Reset()
	{
		Texture2D tex = initialTex;
		Texture2D workTex = new Texture2D(tex.width, tex.height, TextureFormat.ARGB32, false);

		for(int i=0; i< tex.width; i++)
			for (int j=0; j<tex.height; j++)
			{
				workTex.SetPixel(i, j, tex.GetPixel(i, j));

			}

		workTex.Apply();
		Sprite sp = Sprite.Create(workTex, new Rect(0, 0, workTex.width, workTex.height), new Vector2(0, 0));
		img.sprite = sp;
	}




}
