﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewSceneScript : MonoBehaviour {

	public enum Timings {Fastest,Fast,Normal,Slow};
	Dictionary<Timings,WaitForSeconds> timings=new Dictionary<Timings,WaitForSeconds>();
	Animator[] animators;
	GameObject [] parts;

	void Start () {
		animators = GetComponentsInChildren<Animator> (true);
		InitParts ();
		Animate (false);
		InitTimings ();
	}

	public void InitParts()
	{
		int n = transform.childCount;
		parts=new GameObject[n];
		for (int i = 0; i < n; i++)
			parts [i] = transform.GetChild (i).gameObject;
	}

	public void InitTimings()
	{
		timings.Add(Timings.Fastest,null);
		timings.Add(Timings.Fast,new WaitForSeconds(0.1f));
		timings.Add(Timings.Normal,new WaitForSeconds(0.5f));
		timings.Add(Timings.Slow,new WaitForSeconds(1f));
	}
	
	public void Animate(bool animate)
	{
		int n = animators.Length;

		for (int i = 0; i < n; i++)
			animators [i].enabled = animate;
	}

	public IEnumerator ActivateScene(Timings timing,bool activate)
	{
		CanvasGroup cg = GetComponent<CanvasGroup> ();
		//if (activate)
		//	cg.alpha = 1;
	//	else
	//		cg.alpha = 0;
		//yield return null;
		int n = parts.Length;

		for (int i = 0; i < n; i++) {
			parts [i].SetActive (true);
			yield return timings [timing];
		}
	}
}
