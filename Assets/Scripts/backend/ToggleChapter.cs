﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleChapter : MonoBehaviour {
	public Sprite[] sprites;
	public Image checkBox;
	private bool isOn = false;

	void Start () {
		//toggle = GetComponent<Toggle> ();
		//ChangeImage ();
	}

	public bool GetToggle()
	{
		return isOn;
	}

	public void SetToggle(bool isOn)
	{
		this.isOn = isOn;
		ChangeImage();
	}
	
	void ChangeImage()
	{
		if (!isOn) {
			checkBox.sprite = sprites [0];
		} else {
			checkBox.sprite = sprites [1];
		}
	}
}
