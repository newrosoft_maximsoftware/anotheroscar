﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HandleScenes : MonoBehaviour {
	public enum SceneIndex{Loader,Book}

	public void OnAnimationEnd()
	{
		StartCoroutine(LoadSceneAsync());
	}

	IEnumerator LoadSceneAsync()
	{
		AsyncOperation async=SceneManager.LoadSceneAsync((int)SceneIndex.Book,LoadSceneMode.Single);
		async.allowSceneActivation=true;

		while(!async.isDone)
		{
			yield return null;
		}

	}
}
