﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class GestureRecognizer : MonoBehaviour 
{
	public static GestureRecognizer instance;

	public enum SwipeDirection
	{
		Up,
		Down,
		Right,
		Left,
		Center,
		SlideTop,
		SlideBottom
	}

	public event Action<SwipeDirection> Swipe;
	public float swipeSensitivity = 0;
	public RectTransform[] exceptedItems;

	private bool swiping = false;
	private bool eventSent = false;
	private Vector2 lastPosition;
	private bool hasBegin=false;
	float xScreenSize=Screen.width/2.0f;
	SwipeDirection swipeDirection;

	void Start()
	{
		if (instance != null) Destroy(gameObject);
		else instance = this;

		GestureRecognizer.instance.Swipe += BookManager.OnSwipe;

	}


    void Update()
    {

        //Debug swipes in editor
        if (Input.GetKeyDown(KeyCode.U))
        {
            Swipe(SwipeDirection.Down);
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            Swipe(SwipeDirection.Up);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            Swipe(SwipeDirection.Left);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Swipe(SwipeDirection.Right);
        }
        //

        if ((Input.touchCount == 0) || (IsPointerOverUIObject())) return;


        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            lastPosition = Input.GetTouch(0).position;
            hasBegin = true;
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            if (hasBegin)
            {

                Vector2 direction = Input.GetTouch(0).position - lastPosition;
                if (direction.x > swipeSensitivity)
                {
                    Swipe(SwipeDirection.Left);
                    lastPosition = Input.GetTouch(0).position;
                }
                else if (direction.x < -swipeSensitivity)
                {
                    Swipe(SwipeDirection.Right);
                    lastPosition = Input.GetTouch(0).position;
                }
            }
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled)
        {
            //lastPosition = Vector2.zero;
            hasBegin = false;
        }

    }

    //Detects if touch is over UI object with raycast interactable on
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}