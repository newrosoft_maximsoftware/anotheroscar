﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class GuiManagement: MonoBehaviour {

	public static GuiManagement instance;
	private string pageDataPath="xml/GuiData";
	private List<GuiData> scenesData;
	private List<GameObject> cachedScens;
	public bool isDone=true;
	int loadingObjectsCount=0;
	int loadingObjects=0;
    int currentScene = 0;

	void Start () {

		if(instance==null)
			instance=this;
		else Destroy(this);

		cachedScens=new List<GameObject>();
		LoadScenesData();
	}

    public int ScenesCount
    {
        get { return scenesData.Count; }
    }

	public void LoadScenesData()
	{
		scenesData=new List<GuiData>();
		TextAsset textAsset = (TextAsset) Resources.Load(pageDataPath);  
		XmlDocument xmldoc = new XmlDocument ();
		xmldoc.LoadXml ( textAsset.text );

		XmlNodeList scenes=xmldoc.SelectNodes("Scenes/Scene");
		foreach(XmlNode scene in scenes)
		{
			GuiData data=new GuiData();
			data.GuiId=int.Parse(scene.SelectSingleNode("ID").InnerText);
			data.sceneName=scene.SelectSingleNode("Name").InnerText;
			scenesData.Add(data);
		}
	}

	public List<GameObject> getCachedScenes()
	{
		return cachedScens;
	}
		
	private IEnumerator LoadSceneCo(SceneData sceneData)
	{
		ResourceRequest request=Resources.LoadAsync("Prefabs/GUI"+"/"+sceneData.sceneName);

        while(!request.isDone)
            yield return null;
		cachedScens.Add(request.asset as GameObject);
	}

    public IEnumerator LoadRange(int start,int range)
    {
        for (int i = start; i < start + range && i < scenesData.Count; i++)       
            yield return StartCoroutine("LoadSceneCo",scenesData[i]);   
    }

}
