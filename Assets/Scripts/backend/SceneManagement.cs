﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.SceneManagement;

public class SceneManagement: MonoBehaviour{
	public static SceneManagement instance;
	private string pageDataPath="xml/ScenesData";
	private List<SceneData> scenesData;
	private List<string> cachedScens;
	public bool isDone=true;
	int loadingObjectsCount=0;
	int loadingObjects=0;
    int currentScene = 0;

	void Awake()
	{
		

		if (instance == null)
			instance = this;
		else if (instance!=this)
			Destroy(gameObject);

		cachedScens=new List<string>();
		DontDestroyOnLoad (this);
	}

	void Start () {
		LoadScenesData();
	}

    public int ScenesCount
    {
        get { return scenesData.Count; }
    }

	public void LoadScenesData()
	{
		scenesData=new List<SceneData>();
		TextAsset textAsset = (TextAsset) Resources.Load(pageDataPath);  
		XmlDocument xmldoc = new XmlDocument ();
		xmldoc.LoadXml ( textAsset.text );

		XmlNodeList scenes=xmldoc.SelectNodes("Scenes/Scene");
		foreach(XmlNode scene in scenes)
		{
			SceneData data=new SceneData();
			data.chapterNr=int.Parse(scene.SelectSingleNode("Chapter").InnerText);
			data.sceneName=scene.SelectSingleNode("Name").InnerText;
			scenesData.Add(data);
		}
	}

	public List<string> getCachedScenes()
	{
		return cachedScens;
	}
		
	public int ChapterIndex(int chapterNr)
	{
		for(int i=0;i<scenesData.Count;i++)
			if(scenesData[i].chapterNr==chapterNr)
				return i;

		return -1;
	}

	public IEnumerator LoadRange(int start,int range)
	{
		for (int i = start; i < start + range && i < scenesData.Count; i++) {
			yield return StartCoroutine ("LoadSceneCo", i);  
		}
	}

	public IEnumerator LoadSceneCo(int nr)
	{
		AsyncOperation op = SceneManager.LoadSceneAsync (scenesData[nr].sceneName,LoadSceneMode.Additive);
		op.allowSceneActivation = true;

		while (!op.isDone)
			yield return null;

		cachedScens.Add (scenesData [nr].sceneName);
	}

}
