﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GoToCreative : MonoBehaviour {

	public void ChangeScene()
	{
		GuiNavigation.instance.NavigateStoryCreative ();
	}
}
