﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiScript: MonoBehaviour {

	Animator anim;
	private int hashIsPlaying;
	private Parallax parallax;

	public enum Timings {Fastest,Fast,Normal,Slow};
	Dictionary<Timings,WaitForSeconds> timings=new Dictionary<Timings,WaitForSeconds>();
	List<GameObject> parts;
	CanvasGroup canvas;


	void Start () {
		
		Init ();
	}

	void Init()
	{
		canvas = GetComponent<CanvasGroup> ();
		anim = GetComponent<Animator> ();
		hashIsPlaying =Animator.StringToHash("isPlaying");
	}
	
	public void Animate(bool isAnimating)
	{
		anim.SetBool (hashIsPlaying,isAnimating);
	}

	public void ActivateScene(bool isActive)
	{
		/*if (isActive) {
			canvas.alpha = 1;
			canvas.interactable = true;
		} else {
			canvas.alpha = 0;
			canvas.interactable = false;
		}*/
		gameObject.SetActive (isActive);
	}

}
