﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class SceneManagementBackup : MonoBehaviour {

	public static SceneManagementBackup instance;
	private string pageDataPath="xml/ScenesData";
	private List<SceneData> scenesData;
	private List<GameObject> cachedScens;
	public bool isDone=true;
	int loadingObjectsCount=0;
	int loadingObjects=0;
    int currentScene = 0;

	void Start () {

		if(instance==null)
			instance=this;
		else Destroy(this);

		cachedScens=new List<GameObject>();
		LoadScenesData();
	}

    public int ScenesCount
    {
        get { return scenesData.Count; }
    }

	public void LoadScenesData()
	{
		scenesData=new List<SceneData>();
		TextAsset textAsset = (TextAsset) Resources.Load(pageDataPath);  
		XmlDocument xmldoc = new XmlDocument ();
		xmldoc.LoadXml ( textAsset.text );

		XmlNodeList scenes=xmldoc.SelectNodes("Scenes/Scene");
		foreach(XmlNode scene in scenes)
		{
			SceneData data=new SceneData();
			data.chapterNr=int.Parse(scene.SelectSingleNode("Chapter").InnerText);
			data.sceneName=scene.SelectSingleNode("Name").InnerText;
			scenesData.Add(data);
		}
	}

	public List<GameObject> getCachedScenes()
	{
		return cachedScens;
	}



	private List<SceneData> GetChapterData(int chapterNr)
	{
		List<SceneData> chapter=new List<SceneData>();

		foreach(SceneData data in scenesData)
			if(data.chapterNr==chapterNr)
				chapter.Add(data);

		return chapter;
	}

    public void LoadChapter(int chapterNr)
	{
		List<SceneData> chapter=GetChapterData(chapterNr);
		isDone=false;
		loadingObjectsCount=chapter.Count;
		loadingObjects=0;

		foreach(SceneData scene in chapter)
		{
			StartCoroutine("LoadSceneCo",scene);
		}
	}

	private IEnumerator LoadSceneCo(SceneData sceneData)
	{
		ResourceRequest request=Resources.LoadAsync("Prefabs/Chapter"+sceneData.chapterNr+"/"+sceneData.sceneName);

        while(!request.isDone)
            yield return null;
		cachedScens.Add(request.asset as GameObject);
	}

    public IEnumerator LoadRange(int start,int range)
    {
        for (int i = start; i < start + range && i < scenesData.Count; i++)       
            yield return StartCoroutine("LoadSceneCo",scenesData[i]);   
    }

}
