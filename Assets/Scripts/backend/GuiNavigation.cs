﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiNavigation : MonoBehaviour {

	public static GuiNavigation instance;

	void Start()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (this);
	}

	public void NavigateHomeButton()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SetFirstTimeLoad (true);
			BookManager.instance.SwipeBottom (BookManager.instance.GetPage (BookManager.GUIType.Home), BookManager.instance.GetPage (BookManager.GUIType.Navigation));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Navigation);
		}
	}

	public void NavigationCreative()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SetFirstTimeLoad (true);
			BookManager.instance.GetPage (BookManager.GUIType.Navigation).GetComponent<NavigationScene> ().TurnToggleOff ();
			BookManager.instance.SlideTop (BookManager.instance.GetPage (BookManager.GUIType.Navigation), BookManager.instance.GetPage (BookManager.GUIType.GetCreative));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.GetCreative);
		}
	}

	public void NavigateHomeAbout()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SetFirstTimeLoad (true);
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (BookManager.GUIType.Home), BookManager.instance.GetPage (BookManager.GUIType.About));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.About);
		}
	}

	public void NavigateHomeHelp()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SetFirstTimeLoad (true);
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (BookManager.GUIType.Home), BookManager.instance.GetPage (BookManager.GUIType.Help));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Help);
		}
	}

	public void NavigateHomeExtra()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SetFirstTimeLoad (true);
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (BookManager.GUIType.Home), BookManager.instance.GetPage (BookManager.GUIType.ExtraEpic));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.ExtraEpic);
		}
	}

	public void NavigateHomePrivacy()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SetFirstTimeLoad (true);
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (BookManager.GUIType.Home), BookManager.instance.GetPage (BookManager.GUIType.PrivacyPolicy));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.PrivacyPolicy);
		}
	}

	public void NavigatePrivacyHome()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (BookManager.GUIType.PrivacyPolicy), BookManager.instance.GetPage (BookManager.GUIType.Home));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Home);
		}
	}

	public void NavigateExtraHome()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (BookManager.GUIType.ExtraEpic), BookManager.instance.GetPage (BookManager.GUIType.Home));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Home);
		}
	}

	public void NavigateAboutHome()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (BookManager.GUIType.About), BookManager.instance.GetPage (BookManager.GUIType.Home));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Home);
		}
	}

	public void NavigateHelpHome()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (BookManager.GUIType.Help), BookManager.instance.GetPage (BookManager.GUIType.Home));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Home);
		}
	}

	public void NavigateGetCreativeChapters()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SwipeSlideBottom (BookManager.instance.GetPage (BookManager.GUIType.GetCreative), BookManager.instance.GetPage (BookManager.GUIType.Navigation));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Navigation);
		}
	}

	public void NavigateNavigationHome()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SwipeTop (BookManager.instance.GetPage (BookManager.GUIType.Navigation), BookManager.instance.GetPage (BookManager.GUIType.Home));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Home);
		}
	}

	public void NavigateHomeResume()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.HideOverlayButton (true);
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (BookManager.GUIType.Home), BookManager.instance.GetPage ());
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Home);
		}
	}

	public void NavigateHomeStart(int chapter)
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.GetPage (BookManager.GUIType.Navigation).GetComponent<NavigationScene> ().ChapterClicked (chapter);
			//BookManager.instance.LoadScenes (chapter);
		}
	}

	public void NavigateStoryNavigator()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.HideOverlayButton (false);
			BookManager.instance.SwipeSlideBottom (BookManager.instance.GetPage (), BookManager.instance.GetPage (BookManager.GUIType.Navigation));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.Navigation);
		}
	}

	public void NavigateNavigatorReturnButton()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			GameObject ob = BookManager.instance.GetLastKnownPage ();

			if (ob.CompareTag ("Scene")) {
				BookManager.instance.HideOverlayButton (true);
				BookManager.instance.SlideTop (BookManager.instance.GetPage (BookManager.GUIType.Navigation), ob);

			}
			else if (ob.CompareTag ("Gui")) {
				BookManager.instance.SwipeTop (BookManager.instance.GetPage (BookManager.GUIType.Navigation), BookManager.instance.GetPage (BookManager.GUIType.Home));	
				BookManager.instance.SetCurrentGui (BookManager.GUIType.Home);
				BookManager.instance.GetPage (BookManager.GUIType.Navigation).GetComponent<NavigationScene> ().TurnToggleOff ();
			}
		}
	}

	public void NavigateLandingChapter2()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.SetFirstTimeLoad (true);
			BookManager.instance.HideOverlayButton (false);
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (), BookManager.instance.GetPage (BookManager.GUIType.GetCreative));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.GetCreative);
		}
	}

	public void NavigateStoryCreative()
	{
		if (!BookManager.instance.GetIstransitionHappening()) {
			BookManager.instance.HideOverlayButton (false);
			BookManager.instance.SwipeMiddle (BookManager.instance.GetPage (), BookManager.instance.GetPage (BookManager.GUIType.GetCreative));
			BookManager.instance.SetCurrentGui (BookManager.GUIType.GetCreative);
		}
	}

}
