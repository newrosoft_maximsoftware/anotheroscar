﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using UnityEngine.SceneManagement;

public class BookManager : MonoBehaviour 
{
	public enum TransitionMode {Fade, Slide, ChapterFade, ChapterSlide }
	public enum GUIType {Home,Help,About,ExtraEpic,PrivacyPolicy,Navigation,GetCreative}
	public enum SceneType{Scene,Gui	}
	private GUIType currentGui=GUIType.Home;
	private GameObject [] scenes;
	private SceneScript [] scenesScript;
	private Dictionary<GUIType,GameObject> guiScenes;
	private int currentScene = 0;
	public static BookManager instance;

	public GameObject scenesAnimator;
	private Animator transitionAnimator;

	[Header("Transition UI")]
	public GameObject previousPage;
	public GameObject currentPage;
	public GameObject nextPage;
	public GameObject topPage;
	public GameObject bottomPage;
	public GameObject middlePage;
	public string pageDataPath;

	private bool isTransitionHappening = false;
	private int hashLeftSwipe;
	private int hashRightSwipe;
	private int hashTopSwipe;
	private int hashBottomSwipe;
	private int hashMiddleSwipe;
	private int hashSlideBottom;
	private int hashSlideTop;
	private static GestureRecognizer.SwipeDirection swipeDirection;
	public int maxScenes=3;
	public bool canOptimize=true;
	private bool firstTimeLoad=true;

	public GameObject overlayButtons;
	GameObject lastKnownPage;
	Scene unloadScene;


	[Header("UI")]
	public Toggle debugToggle;

	void Awake()
	{
		if (instance == null)
			instance = this;
		//else if (instance != this)
		//	Destroy (gameObject);

		InitGUI();
		
	}
	void Start () 
	{
		transitionAnimator = GetComponent<Animator>();

		hashLeftSwipe = Animator.StringToHash ("leftTransition");
		hashRightSwipe= Animator.StringToHash ("rightTransition");
		hashTopSwipe=Animator.StringToHash ("topTransition");
		hashBottomSwipe=Animator.StringToHash ("bottomTransition");
		hashMiddleSwipe=Animator.StringToHash ("middleTransition");
		hashSlideBottom=Animator.StringToHash ("slideBottomTransition");
		hashSlideTop=Animator.StringToHash ("slideTopTransition");



		//StartCoroutine(Init());
	}

	public void InitGUI()
	{
		guiScenes=new Dictionary<GUIType, GameObject>();
		Transform scenesTransform=scenesAnimator.transform;

		guiScenes.Add(GUIType.Home,scenesTransform.Find("Home").gameObject);
		guiScenes.Add(GUIType.Help,scenesTransform.Find("Help").gameObject);
		guiScenes.Add(GUIType.About,scenesTransform.Find("About").gameObject);
		guiScenes.Add(GUIType.ExtraEpic,scenesTransform.Find("ExtraEpic").gameObject);
		guiScenes.Add(GUIType.PrivacyPolicy,scenesTransform.Find("PrivacyPolicy").gameObject);
		guiScenes.Add(GUIType.Navigation,scenesTransform.Find("Navigation").gameObject);
		guiScenes.Add(GUIType.GetCreative,scenesTransform.Find("GetCreative").gameObject);

		//guiScenes [GUIType.Home].GetComponent<GuiScript> ().Animate (true);
	}

	public void SetFirstTimeLoad(bool firstTime)
	{
		firstTimeLoad = firstTime;
	}

	public void LoadScenes(int chapter)
	{
		firstTimeLoad = false;
		if(scenes==null)
		{
			scenes = new GameObject[SceneManagement.instance.ScenesCount];
			scenesScript=new SceneScript[SceneManagement.instance.ScenesCount];
		}
		/*else
		{
		for(int i=0;i<scenes.Length;i++)
			if(scenes[i]!=null)
			{
				Destroy(scenes[i]);
			}
		}*/
		StartCoroutine(LoadScenesCo(chapter));
	}

	IEnumerator LoadScenesCo(int chapter)
	{
		int chapterIndex=SceneManagement.instance.ChapterIndex(chapter);
		int startingIndex=chapterIndex-maxScenes/2;
		currentScene=chapterIndex;

		if(chapter==1)			
		{
			yield return StartCoroutine("InstantiateScenes",new int [] {0,maxScenes});
		}
		else
		{
			yield return StartCoroutine("InstantiateScenes",new int [] {startingIndex,maxScenes});
		}

		lastKnownPage = scenes [currentScene];

		scenes [currentScene].transform.SetAsLastSibling ();
		scenesScript[currentScene].ActivateScene(true);
		yield return null;
		scenesScript[currentScene].Animate(true);
		scenesScript[currentScene].EnableParallax(true);
		guiScenes[currentGui].GetComponent<GuiScript> ().ActivateScene(false);
		overlayButtons.SetActive (true);
	}

	IEnumerator InstantiateScenes(int [] param)
	{
		firstTimeLoad = false;
		yield return SceneManagement.instance.LoadRange(param[0],param[1]);			
		List<string> cachedScenes =SceneManagement.instance.getCachedScenes();

		int poz = param[0];
		while (cachedScenes.Count > 0)
		{
			Scene scena = SceneManager.GetSceneByName (cachedScenes [0]);
			GameObject[] arr = scena.GetRootGameObjects ();
			if(arr.Length>0)
			{
				GameObject obj = arr [0].transform.GetChild (0).gameObject;
			obj.transform.SetParent(scenesAnimator.transform);
			obj.transform.localPosition = Vector3.zero;
			scenes[poz]=obj;
			scenesScript[poz]=obj.GetComponent<SceneScript>();
			poz++;
				if (cachedScenes.Count > 1)
					SceneManager.UnloadSceneAsync (cachedScenes [0]);
				else
					unloadScene = scena;
			cachedScenes.RemoveAt(0);
			yield return new WaitForSeconds(0.0f);

			//obj.SetActive (true);
			}
		}
	}


	void CleanScene(int index)
	{
			//SceneManager.MoveGameObjectToScene (scenes[index],unloadScene);
		if(currentScene-2>=0 && scenes[currentScene-2]!=null)
			scenes [index].transform.SetParent (unloadScene.GetRootGameObjects()[0].transform);
			SceneManager.UnloadSceneAsync (unloadScene);
	}



	public static void OnSwipe(GestureRecognizer.SwipeDirection d)
	{
		//Debug.LogError("Swipe " + d);
		TransitionMode t = TransitionMode.Slide;
		switch(d)
		{
		case GestureRecognizer.SwipeDirection.Left:
			swipeDirection = GestureRecognizer.SwipeDirection.Left;
			instance.PreviousScene(t);

			break;
		case GestureRecognizer.SwipeDirection.Right:
			swipeDirection = GestureRecognizer.SwipeDirection.Right;
			instance.NextScene(t);
			break;
		default:
			break;
		}
	}


	void GetPageData()
	{
		TextAsset textAsset = (TextAsset) Resources.Load(pageDataPath);  
		XmlDocument xmldoc = new XmlDocument ();
		xmldoc.LoadXml ( textAsset.text );
	}


	public void NextScene(TransitionMode transition)
	{
		if (isTransitionHappening) return;
		if (currentScene<scenes.Length-1)
		{

			if (currentScene+2 < scenes.Length && scenes[currentScene + 2] == null)
			{
				StartCoroutine("InstantiateScenes", new int[] { currentScene + 2, 1 });
				CleanScene(currentScene - 2);
			}


			SetupTransition(null,scenes[currentScene],scenes[currentScene+1],null,null,null);
			isTransitionHappening = true;
			currentScene++;

			lastKnownPage = scenes [currentScene];
			transitionAnimator.SetTrigger(hashRightSwipe);  

		}
		// }
	}


	public void PreviousScene(TransitionMode transition)
	{
		if (isTransitionHappening) return;

		if (currentScene >0)
		{			
			if (currentScene-2>=0 && scenes[currentScene - 2] == null)
			{
				StartCoroutine("InstantiateScenes", new int[] { currentScene - 2, 1 });
				CleanScene(currentScene + 2);
			}

			SetupTransition(scenes[currentScene-1],scenes[currentScene],null);
			isTransitionHappening = true;
			currentScene--;
			lastKnownPage = scenes [currentScene];
			transitionAnimator.SetTrigger(hashLeftSwipe);
		}
	}

	public GameObject GetPage(GUIType type)
	{
		return guiScenes[type];

	}

	public GameObject GetPage()
	{
		return scenes [currentScene];

	}

	public void HideOverlayButton(bool isActive)
	{
		overlayButtons.SetActive (isActive);
	}


	public void SlideTop(GameObject scene1,GameObject scene2)
	{
		if (isTransitionHappening) return;
		lastKnownPage = scene1;
		CheckHome ();
		SetupTransition (null,scene1,null,null,scene2,null);
		swipeDirection = GestureRecognizer.SwipeDirection.SlideTop;
		isTransitionHappening = true;
		transitionAnimator.SetTrigger(hashSlideTop);
	}

	public void SwipeSlideBottom(GameObject scene1,GameObject scene2)
	{
		if (isTransitionHappening) return;
		lastKnownPage = scene1;
		CheckHome ();
		SetupTransition (null,scene1,null,scene2,null,null);
		swipeDirection = GestureRecognizer.SwipeDirection.SlideBottom;
		isTransitionHappening = true;
		transitionAnimator.SetTrigger(hashSlideBottom);
	}

	public void SwipeTop(GameObject scene1,GameObject scene2)
	{
		if (isTransitionHappening) return;
		lastKnownPage = scene1;
		CheckHome ();
		SetupTransition (null,scene1,null,null,null,scene2);
		swipeDirection = GestureRecognizer.SwipeDirection.Up;
		isTransitionHappening = true;
		transitionAnimator.SetTrigger(hashTopSwipe);
	}

	public void SwipeMiddle(GameObject scene1,GameObject scene2)
	{
		if (isTransitionHappening) return;
		lastKnownPage = scene1;
		CheckHome ();
		SetupTransition (null,scene1,null,null,null,scene2);
		swipeDirection = GestureRecognizer.SwipeDirection.Center;
		isTransitionHappening = true;
		transitionAnimator.SetTrigger(hashMiddleSwipe);
	}

	public void  CheckHome()
	{
		if (currentGui == GUIType.Home) {
			GameObject go=guiScenes[currentGui].transform.Find("UI/Buttons/FirstTimeLoad").gameObject;
			GameObject go1=guiScenes[currentGui].transform.Find("UI/Buttons/MultipleTimeLoad").gameObject;

			if(firstTimeLoad==true)
			{
				go.SetActive(true);
				go1.SetActive(false);
			}
			else
			{
				go.SetActive(false);
				go1.SetActive(true);

			}
		}
	}


	public void SwipeBottom(GameObject scene1,GameObject scene2)
	{
		if (isTransitionHappening) return;
		lastKnownPage = scene1;
		CheckHome ();
		SceneType scene1Type=(SceneType)System.Enum.Parse(typeof(SceneType),scene1.tag);
		SceneType scene2Type=(SceneType)System.Enum.Parse(typeof(SceneType),scene2.tag);

		SetupTransition (null,scene1,null,scene2,null,null);
		swipeDirection = GestureRecognizer.SwipeDirection.Down;
		isTransitionHappening = true;
		transitionAnimator.SetTrigger(hashBottomSwipe);
	}

	public bool GetIstransitionHappening()
	{
		return isTransitionHappening;
	}

	public void SetCurrentGui(GUIType type)
	{
		currentGui = type;
	}

	public GameObject GetLastKnownPage()
	{
		return lastKnownPage;
	}

	private Transform MoveObjectFromViewport(GameObject obj)
	{
		Transform t1 = null;
		if (obj.transform.childCount > 0) 
		{
			t1 = obj.transform.GetChild (0).transform;
			t1.parent = scenesAnimator.transform;
			t1.localPosition = Vector3.zero;
		}
		return t1;
	}

	IEnumerator Calc(GuiScript script)
	{
		script.Animate(false);
		yield return new WaitForSeconds (0.05f);
		script.ActivateScene (false);
		isTransitionHappening = false;
	}

	IEnumerator Calc(SceneScript script)
	{
		script.Animate(false);
		yield return new WaitForSeconds (0.05f);
		script.ActivateScene (false);
		isTransitionHappening = false;
	}

	public void EndTransitionOperations(Transform t1,Transform t2)
	{
		if (t1.CompareTag ("Scene")) {
			scenesScript[currentScene].Animate(true);
			scenesScript[currentScene].EnableParallax(true);
		}
		else if(t1.CompareTag ("Gui"))
		{
			t1.SetSiblingIndex (scenesAnimator.transform.childCount-1);
			guiScenes [currentGui].GetComponent<GuiScript> ().Animate (true);
		}

		if (t2.CompareTag ("Scene")) {
			//scenesScript [currentScene].GetComponent<SceneScript> ().ActivateScene(false);
			//StartCoroutine(scenesScript [currentScene].GetComponent<SceneScript> ());
			SceneScript script=scenesScript [currentScene].GetComponent<SceneScript> ();
			script.ActivateScene (false);
		}
		else if(t2.CompareTag ("Gui"))
		{
			//t2.GetComponent<GuiScript> ().Animate(false);
			//t2.GetComponent<GuiScript> ().ActivateScene(false);
			GuiScript script=t2.GetComponent<GuiScript> ();
			script.ActivateScene (false);
		}
	}

	public void BeforeTransitionOperations(Transform t1,Transform t2)
	{
		if (t1.CompareTag ("Scene")) {
			t1.GetComponentInChildren<SceneScript> ().Animate (false);
		}
		else if(t1.CompareTag ("Gui"))
		{
			t1.GetComponentInChildren<GuiScript> ().Animate (false);
		}

		if (t2.CompareTag ("Scene")) {
			t2.GetComponentInChildren<SceneScript> ().Animate (false);
		}
		else if(t2.CompareTag ("Gui"))
		{
			t2.GetComponentInChildren<GuiScript> ().Animate (false);
		}
	}

	private void AnotherOnTransitionEnd()
	{
		Transform t1=MoveObjectFromViewport(topPage);
		Transform t2=MoveObjectFromViewport(middlePage);
		Transform t3=MoveObjectFromViewport(currentPage);
		Transform t4=MoveObjectFromViewport(bottomPage);

		isTransitionHappening = false;

		switch (swipeDirection) {
		case GestureRecognizer.SwipeDirection.Down:
			EndTransitionOperations (t1,t3);
			break;
		case GestureRecognizer.SwipeDirection.Center:
			EndTransitionOperations (t2,t3);
			break;
		case GestureRecognizer.SwipeDirection.Up:
			EndTransitionOperations (t2,t3);
			break;
		case GestureRecognizer.SwipeDirection.SlideBottom:
			EndTransitionOperations (t1,t3);
			break;
		case GestureRecognizer.SwipeDirection.SlideTop:
			EndTransitionOperations (t4,t3);
			break;
		}

	}


	public void SetupTransition(GameObject scene1 = null, GameObject scene2 = null, GameObject scene3 = null, GameObject scene4 = null, GameObject scene5 = null, GameObject scene6 = null)
	{
		if (debugToggle.isOn) 
		{
			debugToggle.isOn = false;
			ToggleDebug();
		}

		if (scene2 != null)
		{
			scene2.transform.SetParent(currentPage.transform);
			scene2.transform.localPosition = Vector3.zero;


			if (scene2.CompareTag ("Scene")) {
				SceneScript script = scene2.GetComponent<SceneScript> ();
				script.EnableParallax (false);
				script.ActivateScene (true);
			}
			else if(scene2.CompareTag ("Gui")) {
				
				//scene2.SetActive (true);
				scene2.GetComponent<GuiScript> ().ActivateScene(true);
			}
		}
		if (scene3 != null)
		{
			scene3.transform.parent = nextPage.transform;
			scene3.transform.localPosition = Vector3.zero;

			if (scene3.CompareTag ("Scene")) {
				SceneScript script = scene3.GetComponent<SceneScript> ();
				script.EnableParallax (false);
				script.ActivateScene (true);
			}
			else if(scene3.CompareTag ("Gui")) {
				
				//scene3.SetActive (true);
				scene3.GetComponent<GuiScript> ().ActivateScene(true);
			}
		}

		if (scene1 != null)
		{
			scene1.transform.parent = previousPage.transform;
			scene1.transform.localPosition = Vector3.zero;

			if (scene1.CompareTag ("Scene")) {
				SceneScript script = scene1.GetComponent<SceneScript> ();
				script.EnableParallax (false);
				script.ActivateScene (true);
			}
			else if(scene1.CompareTag ("Gui")) {
				
				scene1.GetComponent<GuiScript> ().ActivateScene(true);
			}
		}

		if (scene4 != null)
		{
			scene4.transform.parent = topPage.transform;
			scene4.transform.localPosition = Vector3.zero;

			if (scene4.CompareTag ("Scene")) {
				SceneScript script = scene4.GetComponent<SceneScript> ();
				script.EnableParallax (false);
				script.ActivateScene (true);
			}
			else if(scene4.CompareTag ("Gui")) {
				
				scene4.GetComponent<GuiScript> ().ActivateScene(true);
			}
		}

		if (scene5 != null)
		{
			scene5.transform.parent = bottomPage.transform;
			scene5.transform.localPosition = Vector3.zero;

			if (scene5.CompareTag ("Scene")) {
				SceneScript script = scene5.GetComponent<SceneScript> ();
				script.EnableParallax (false);
				script.ActivateScene (true);
			}
			else if(scene5.CompareTag ("Gui")) {
				
				scene5.GetComponent<GuiScript> ().ActivateScene(true);
			}
		}

		if (scene6 != null)
		{
			scene6.transform.parent = middlePage.transform;
			scene6.transform.localPosition = Vector3.zero;

			if (scene6.CompareTag ("Scene")) {
				SceneScript script = scene6.GetComponent<SceneScript> ();
				script.EnableParallax (false);
				script.ActivateScene (true);
			}
			else if(scene6.CompareTag ("Gui")) {
				
				scene6.GetComponent<GuiScript> ().ActivateScene(true);
			}
		}
	}


	Transform GetTransform(GameObject go)
	{
		Transform tr = null;
		if(go.transform.childCount>0)
			tr=go.transform.GetChild(0);
		return tr;
	}


	private void OnBeforeOtherTransitionEnd()
	{
		Transform t1=GetTransform(topPage);
		Transform t2=GetTransform(middlePage);
		Transform t3=GetTransform(currentPage);
		Transform t4=GetTransform(bottomPage);

		switch (swipeDirection) {
		case GestureRecognizer.SwipeDirection.Down:
			BeforeTransitionOperations (t1,t3);
			break;
		case GestureRecognizer.SwipeDirection.Center:
			BeforeTransitionOperations (t2,t3);
			break;
		case GestureRecognizer.SwipeDirection.Up:
			BeforeTransitionOperations (t2,t3);
			break;
		case GestureRecognizer.SwipeDirection.SlideBottom:
			BeforeTransitionOperations (t1,t3);
			break;
		case GestureRecognizer.SwipeDirection.SlideTop:
			BeforeTransitionOperations (t4,t3);
			break;
		}
	}

	private void OnBeforeTransitionEnd()
	{
		if (swipeDirection==GestureRecognizer.SwipeDirection.Right)
			scenesScript [currentScene - 1].Animate(false);
		else 
			scenesScript [currentScene + 1].Animate(false);
	}



	private void OnTransitionEnd()
	{
		Transform t2 = currentPage.transform.GetChild(0).transform;
		t2.parent = scenesAnimator.transform;
		t2.localPosition = Vector3.zero;

		//ResetPreviousStateSprites();
		if (previousPage.transform.childCount > 0)
		{
			Transform t1 = previousPage.transform.GetChild (0).transform;
			t1.parent = scenesAnimator.transform;
			t1.localPosition = Vector3.zero;
		}


		//currentPage.transform.GetChild(0).transform.position = Vector3.zero;

		if (nextPage.transform.childCount > 0) 
		{
			Transform t3 = nextPage.transform.GetChild (0).transform;
			t3.parent = scenesAnimator.transform;
			t3.localPosition = Vector3.zero;
			//nextPage.transform.GetChild (0).transform.position = Vector3.zero;
		}


		//[NRS]
		scenesScript[currentScene].Animate(true);


		if (swipeDirection==GestureRecognizer.SwipeDirection.Right)
			scenesScript [currentScene - 1].ActivateScene (false);
		else
			scenesScript [currentScene + 1].ActivateScene (false);

		isTransitionHappening = false;
		scenesScript[currentScene].EnableParallax(true);
	}

	public void ToggleDebug()
	{

		scenes[currentScene].transform.Find("mockup").gameObject.SetActive(debugToggle.isOn);
	}

}
