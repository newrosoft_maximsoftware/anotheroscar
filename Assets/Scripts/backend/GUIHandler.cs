﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIHandler: MonoBehaviour {

	public static GUIHandler instance;


	void Awake()
	{
		if(instance==null)
			instance=this;
		else if(instance!=this)
			Destroy(this);
	}

}
