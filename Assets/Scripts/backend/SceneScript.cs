﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneScript: MonoBehaviour {

	protected Animator anim;
	private int hashIsPlaying;
	private Parallax parallax;

	public enum Timings {Fastest,Fast,Normal,Slow};
	Dictionary<Timings,WaitForSeconds> timings=new Dictionary<Timings,WaitForSeconds>();
	List<GameObject> parts;
	CanvasGroup canvas;
	bool defaultState=false;


	public void Start () {
		
		Init ();
	}

	public bool GetDefaultState()
	{
		return defaultState;
	}

	void Init()
	{
		InitParts ();
		InitTimings ();
		canvas = GetComponent<CanvasGroup> ();
		anim = GetComponent<Animator> ();
		hashIsPlaying =Animator.StringToHash("isPlaying");
		parallax=GetComponent<Parallax>();
	}


	public void InitParts()
	{
		int n = transform.childCount;
		parts=new List<GameObject>();
		for (int i = 0; i < n; i++) {
			GameObject obj=transform.GetChild (i).gameObject;
			if (!obj.name.Equals ("mockup"))
				parts.Add(obj);
		}
		parts.Add(gameObject);
	}

	public void InitTimings()
	{
		timings.Add(Timings.Fastest,null);
		timings.Add(Timings.Fast,new WaitForSeconds(0.1f));
		timings.Add(Timings.Normal,new WaitForSeconds(0.5f));
		timings.Add(Timings.Slow,new WaitForSeconds(1f));
	}

	public void EnableParallax(bool isEnabled)
	{
		if(parallax!=null)
			parallax.enabled=isEnabled;
	}

	public void SetDefaultState()
	{
		defaultState = false;
	}
	
	public void Animate(bool isAnimating)
	{
		if (isAnimating == false)
			defaultState = true;
		anim.SetBool (hashIsPlaying,isAnimating);
	}

	public void EnableAnimator(bool isEnabled){
		if (anim.isActiveAndEnabled != isEnabled)
			anim.enabled = isEnabled;
	}

	public IEnumerator ActivateScene(Timings timing,bool activate)
	{

		int n = parts.Count;

		for (int i = 0; i < n; i++) {
			yield return timings [timing];
			parts [i].SetActive (activate);

		}

	}

	public void ActivateScene(bool isActive)
	{
		/*if (isActive) {
			canvas.alpha = 1;
			canvas.interactable = true;
		} else {
			canvas.alpha = 0;
			canvas.interactable = false;
		}
	}*/
		gameObject.SetActive (isActive);
		
		/*if (parts == null)
			Init ();

		int n = parts.Count;

		for (int i = 0; i < n; i++) {
			parts [i].SetActive (isActive);

		}*/
	}
		}
