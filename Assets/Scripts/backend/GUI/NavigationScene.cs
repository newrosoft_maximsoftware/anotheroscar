﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationScene : GuiScript {

	public ToggleChapter[] chapters;


	public void ChapterClicked(int chapter)
	{
		int poz = chapter - 1;
		if (!chapters [poz].GetToggle ()) {
			TurnToggleOff (poz);
			chapters [poz].SetToggle (true);
			BookManager.instance.LoadScenes (chapter);
		}
	}

	public void TurnToggleOff()
	{
		for (int i = 0; i < chapters.Length; i++)
			chapters [i].SetToggle (false);
	}

	public void TurnToggleOff(int poz)
	{
		for (int i = 0; i < chapters.Length; i++)
			if(poz!=i)
				chapters [i].SetToggle (false);
	}
}
