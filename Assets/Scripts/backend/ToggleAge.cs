﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleAge : MonoBehaviour {
	public Sprite[] sprites;
	public Image checkBox;
	Toggle toggle;

	void Start () {
		toggle = GetComponent<Toggle> ();
	}

	public void AgeChecked()
	{
		ChangeImage ();
	}
	
	void ChangeImage()
	{
		if (!toggle.isOn) {
			checkBox.sprite = sprites [0];
		} else {
			checkBox.sprite = sprites [1];
		}
	}
}
