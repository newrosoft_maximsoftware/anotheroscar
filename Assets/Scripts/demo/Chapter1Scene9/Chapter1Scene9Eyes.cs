﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter1Scene9Eyes : MonoBehaviour {

	public Chapter1Scene9Door door;
	public Animator uiAnimator;
	private Animator anim;

	void Awake()
	{
		anim = GetComponent<Animator> ();
	}

	public void TriggerUi()
	{
		//door.SetIsGlowing (true);
		//uiAnimator.SetTrigger ("nextUi");
		StartCoroutine(TriggerUiCo());
	}

	private IEnumerator TriggerUiCo()
	{
		door.SetIsGlowing (true);
		uiAnimator.SetTrigger ("nextUi");
		yield return null;
		//yield return new WaitForSeconds (0.1f);
		//door.SetCanBePressed (true);
	}

	public void Animate()
	{
		anim.SetTrigger("isShowing");
	}

	public void Rebind()
	{
		anim.Rebind ();
	}

}
