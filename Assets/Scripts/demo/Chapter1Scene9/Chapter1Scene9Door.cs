﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Chapter1Scene9Door : MonoBehaviour {


	private bool isOpened=false;
	private int animationProgress = 0;


	public Image maze;
	public Image space;
	public Image safari;

	public float coolDown = 3f;
	public bool canBePressed = false;

	private Animator anim;
	public Animator uiAnimator;
	public Animator comet;
	public Animator giraffe;
	public Chapter1Scene9Eyes eyes;
	private bool wasDisabled = false;

	private IEnumerator coroutineRestart;
	private IEnumerator coroutineSafari;
	private IEnumerator coroutineComet;
	private IEnumerator coroutineWait;

	void Awake()
	{
		anim = GetComponent<Animator> ();
	}

	void Start()
	{
		
		coroutineRestart = Restart ();
		coroutineSafari = StartSafariCo ();
		coroutineComet = StartComet ();
		coroutineWait = WaitForProgress ();
	}

	void OnEnable()
	{
		if (wasDisabled) {
			//StopCoroutine (coroutineRestart);
			StopCoroutine (coroutineSafari);
			StopCoroutine (coroutineComet);
			StopCoroutine (coroutineWait);

			coroutineRestart = Restart ();
			coroutineSafari = StartSafariCo ();
			coroutineComet = StartComet ();
			coroutineWait = WaitForProgress ();

			if (wasDisabled) {
				wasDisabled = false;
				StartCoroutine (SwipeOut ());
			}
		}

	}

	private IEnumerator SwipeOut()
	{
		yield return new WaitForSeconds(0.1f);
		anim.SetBool ("isGlowing", false);
		anim.SetBool ("isOpened", false);
		giraffe.SetBool ("start",false);
		animationProgress = 0;
		isOpened = false;
		canBePressed = false;
		maze.enabled = false;
		space.enabled = false;
		safari.enabled = false;
		wasDisabled = false;
	}

	private IEnumerator WaitForProgress()
	{
		yield return new WaitForSeconds (0.2f);

		animationProgress = 0;
	}


		void OnDisable()
	{

		wasDisabled = true;
	}

	public void ToggleDoor()
	{
		if (canBePressed) 
		{
			isOpened = !isOpened;
			anim.SetBool ("isOpened", isOpened);
			//canBePressed = false;

			ToggleMethod ();
		}
	}

	//IEnumerator ResetCooldown()
	//{
	//	yield return new WaitForSeconds (coolDown);
	//	canBePressed = true;
	//}



	public void SetCanBePressed(bool val)
	{
		canBePressed = val;

	}

	public void SetIsGlowing(bool val)
	{
		anim.SetBool ("isGlowing",val);
		if (val)
			canBePressed = true;
		else
			canBePressed = false;
	}

	public IEnumerator StartComet()
	{
		//SetIsGlowing (false);
		//yield return new WaitForSeconds (0.5f);
		//space.enabled = false;
		//canBePressed = true;
                  		//ToggleDoor ();
		yield return new WaitForSeconds (1f);
		comet.SetTrigger ("start");
		uiAnimator.SetTrigger ("nextUi");
		yield return new WaitForSeconds (1.3f);
		SetIsGlowing (true);
		//yield return new WaitForSeconds (0.1f);
		//canBePressed = true;
	}

	public IEnumerator StartSafariCo()
	{
		//safari.enabled = false;
		SetIsGlowing (false);
		giraffe.SetBool ("start",true);
		yield return new WaitForSeconds (1f);
		//canBePressed = true;
		ToggleDoor ();
		uiAnimator.SetTrigger ("nextUi");
		yield return new WaitForSeconds (2f);
		SetIsGlowing (true);
		//canBePressed = true;

	}

	public IEnumerator Restart()
	{
		
		coroutineRestart = Restart ();
		coroutineSafari = StartSafariCo ();
		coroutineComet = StartComet ();
		coroutineWait = WaitForProgress ();

		uiAnimator.SetTrigger ("nextUi");
		SetIsGlowing (false);
		yield return new WaitForSeconds (1f);
		giraffe.SetBool ("start",false);
		animationProgress = 0;
		maze.enabled = false;
		space.enabled = false;
		safari.enabled = false;
        SetIsGlowing(true);
        canBePressed = true;
	}

	public IEnumerator WaitForReopen()
	{
		canBePressed = false;
		yield return new WaitForSeconds (0.4f);
		canBePressed = true;
		ToggleDoor ();
		SetIsGlowing(false);
	}

	public void ToggleMethod()
	{
		if (isOpened) {
			if(animationProgress==0)
				{
					maze.enabled = true;
					SetIsGlowing (false);
					eyes.Animate ();	
				}
			else if(animationProgress==1)
				{
					maze.enabled = false;
					space.enabled = true;
					uiAnimator.SetTrigger ("nextUi");
					StartCoroutine (coroutineComet);
				}
			else if(animationProgress==2)
				{
					space.enabled = false;
					safari.enabled = true;
					StartCoroutine (coroutineSafari);
				}
		} else if (!isOpened) {

			if (animationProgress == 0) {
				animationProgress++;
				StartCoroutine (WaitForReopen ());
			} else if (animationProgress == 1) {
				animationProgress++;
				StartCoroutine (WaitForReopen ());
			} else if (animationProgress == 2) {
				StartCoroutine (coroutineRestart);
			} 

		}
	}
}
