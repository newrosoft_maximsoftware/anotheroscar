﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch : MonoBehaviour 
{
	private bool isLightOn = true;

	private bool wasSwitchedOff = false;

	public Image lightOnImage;
	public Image lightOffImage;

	void OnDisable()
	{
		if (!isLightOn) ToggleLight();
	}

	public void ToggleLight()
	{
		isLightOn = !isLightOn;
		lightOffImage.gameObject.SetActive(!isLightOn);
		lightOnImage.gameObject.SetActive(isLightOn);



	}
}
