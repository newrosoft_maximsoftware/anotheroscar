﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchPotts : MonoBehaviour {
	public Animator anim;
	// Use this for initialization
	void Start () {
		
	}
	

	public void SetPotsBool()
	{
		anim.SetBool("touched", true);
	}

	public void Reset()
	{
		anim.SetBool("touched", false);
	}

	void OnEnable()
	{
		Reset ();
	}
}
