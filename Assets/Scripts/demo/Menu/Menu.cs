﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void LoadScene(string name)
	{
		SceneManager.LoadScene(name);
	}

	public void ReloadScene()
	{
		Debug.Log("reload");
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
