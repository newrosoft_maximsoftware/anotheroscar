﻿using UnityEngine;
using System.Collections;

public class Seagull : MonoBehaviour {
	private Animator anim;
	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
		InvokeRepeating("GenerateRandomNum",0f, 3f); 
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	private void GenerateRandomNum()
	{
		int n = Random.Range(0,2);
		if (n==1)
			anim.SetTrigger("turnHead");
			
	}

	void OnMouseDown() 
	{
		anim.SetTrigger("turnHead");
	}
}
