﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScenarioManager : MonoBehaviour 
{
	[HideInInspector]
	public static ScenarioManager instance;

	[Header("General")]
	public Text[] texts;
	public int step = 0;

	void Start () 
	{
		if (instance == null) instance = this;
		else Destroy(gameObject);

		switch (SceneManager.GetActiveScene().name)
		{
			case "scene1":
				StartCoroutine(Story1());
				break;

			case "scene2":
			//texts[0].GetComponent<Animator>().SetTrigger("fade");
				StartCoroutine(Story2());
				break;

		}

	
	}
	



	#region Scene5

	IEnumerator Story1()
	{
		foreach(Text t in texts)
		{
			t.GetComponent<Animator>().SetTrigger("fade");
			yield return new WaitForSeconds(3.5f);
		}

	}

	#endregion

	#region Scene25

	[Header("Scene25")]
	public Animator potatoKingBubble;

	IEnumerator Story2()
	{
		texts[0].GetComponent<Animator>().SetTrigger("fade");
		while (step < 1) yield return null;

		yield return new WaitForSeconds(1f);
		texts[1].GetComponent<Animator>().SetTrigger("fade");
		yield return new WaitForSeconds(3.5f);
		texts[2].GetComponent<Animator>().SetTrigger("fade");
		yield return new WaitForSeconds(3.5f);
		potatoKingBubble.SetTrigger("fade");
	}

	#endregion
}
