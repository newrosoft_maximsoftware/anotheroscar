﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Parallax : MonoBehaviour {
	public Transform[] backgrounds;
	public float smoothing;
	public float accSmoothing = 1f;

	private Vector3 center;
	private float[] parallaxScales;
	private Vector3[] targetPositions;
	private Vector3[] initialPositions;
	private Vector3 previousAccelerometerValue;
	public Vector2 offset = Vector2.zero;

	//filter
	protected Queue<Vector3> filterDataQueue = new Queue<Vector3>();
	public int filterLength = 3; //you could change it in inspector


	// Use this for initialization
	void Start () 
	{
		for(int i=0; i<filterLength; i++)
			filterDataQueue.Enqueue(Input.acceleration);

		previousAccelerometerValue = Input.acceleration;
		Vector3 centerScr = new Vector3(Screen.width/2, Screen.height/2, 0f);
		center = Camera.main.ScreenToWorldPoint(centerScr);

		parallaxScales = new float[backgrounds.Length];
		targetPositions = new Vector3[backgrounds.Length];
		initialPositions = new Vector3[backgrounds.Length];

		for (int i=0; i<parallaxScales.Length; i++)
		{
			parallaxScales[i] = backgrounds[i].position.z ;//* -1;
			initialPositions[i] = backgrounds[i].position;
		}
	}

	public Vector3 LowPassAccelerometer() 
	{
		if(filterLength <= 0)
			return Input.acceleration;
		filterDataQueue.Enqueue(Input.acceleration);
		filterDataQueue.Dequeue();

		Vector3 vFiltered= Vector3.zero;
		foreach(Vector3 v in filterDataQueue)
			vFiltered += v;
		vFiltered /= filterLength;
		return vFiltered;
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		//Debug.Log(Input.acceleration);
		for (int i=0; i<backgrounds.Length; i++)
		{
			//Vector3 parallax = (previousAccelerometerValue - Input.acceleration) * (parallaxScales[i]/smoothing);
			Vector3 parallax = -LowPassAccelerometer() * parallaxScales[i]/smoothing;

			targetPositions[i].Set( (center.x + parallax.x + offset.x), backgrounds[i].transform.position.y + offset.y, backgrounds[i].transform.position.z);

			backgrounds[i].transform.position = Vector3.Lerp(backgrounds[i].transform.position, targetPositions[i], Time.time * smoothing);

		} 

		//previousAccelerometerValue = Input.acceleration;
	}
}
