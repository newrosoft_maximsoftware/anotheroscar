﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimateElements : MonoBehaviour {
	private enum State{None,Paused,Resumed};
	private State currentState=State.None;
	public UnityEvent animateSquid;
	public UnityEvent animateWindow;
	public UnityEvent animateWaterSpray;
	public UnityEvent animateRider;
	[Header("Scene6")]
	public UnityEvent animateClock;
	public UnityEvent mumToggleLight;

	[Header("Scene9")]
	public UnityEvent doorClickable;

	[Header("Scene20")]
	public UnityEvent mrPotsClickable;

	[Header("Scene30")]
	public UnityEvent animateButterfly1;
	public UnityEvent animateButterfly2;
	public UnityEvent animateButterfly3;
	public UnityEvent animateButterfly4;

	[Header("Scene30")]
	public UnityEvent animatePauseButton;
	//SCENE32

	[Header("Scene42")]
	public UnityEvent animaterReflection1;
	public UnityEvent animaterReflection2;
	public UnityEvent animaterReflection3;
	public UnityEvent animaterReflection4;

	private Animator anim;

	void Start()
	{
		anim = GetComponent<Animator>();
	}

	public void AnimateSquid()
	{
		animateSquid.Invoke();
	
	}

	public void AnimateWindow()
	{
		animateWindow.Invoke();
	}

	public void AnimateWaterSpray()
	{
		animateWaterSpray.Invoke();
	}

	public void AnimateRider()
	{
		animateRider.Invoke();
	}

	public void AnimateClock()
	{
		animateClock.Invoke();
	}
	public void MumToggleLight()
	{
		mumToggleLight.Invoke();
	}

	public void SetDoorClickable()
	{
		doorClickable.Invoke ();
	}

	public void MrPotsClickable()
	{
		mrPotsClickable.Invoke ();
	}

	public void AnimateButterfly1()
	{
		animateButterfly1.Invoke ();
	}

	public void AnimateButterfly2()
	{
		animateButterfly2.Invoke ();
	}

	public void AnimateButterfly3()
	{
		animateButterfly3.Invoke ();
	}

	public void AnimateButterfly4()
	{
		animateButterfly4.Invoke ();
	}

	public void AnimatePauseButton()
	{
		animatePauseButton.Invoke ();
	}

	public void Pause()
	{
		if (currentState == State.None) {
			anim.enabled = false;
			currentState = State.Paused;
		}
	}

	public void Resume()
	{
		anim.enabled = true;
		currentState = State.Resumed;
	}

	public void AnimateReflection1()
	{
		animaterReflection1.Invoke ();
	}

	public void AnimateReflection2()
	{
		animaterReflection2.Invoke ();
	}

	public void AnimateReflection3()
	{
		animaterReflection3.Invoke ();
	}

	public void AnimateReflection4()
	{
		animaterReflection4.Invoke ();
	}




}
