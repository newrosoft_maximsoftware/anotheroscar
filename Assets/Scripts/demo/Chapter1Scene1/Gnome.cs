﻿using UnityEngine;
using System.Collections;

public class Gnome : MonoBehaviour {
	public EdgeCollider2D ground;
	public float pushForce;

	[Header("Bounds")]
	public Vector2[] bounds;

	private Rigidbody2D rbd;
	private PolygonCollider2D col;
	private Vector3 prevMousePosition;

	// Use this for initialization
	void Start () {

		col = GetComponent<PolygonCollider2D>();
		rbd = GetComponent<Rigidbody2D>();	
		prevMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}

	void Update()
	{
		Debug.DrawLine(new Vector3(bounds[0].x, bounds[0].y, 0f), new Vector3(bounds[1].x, bounds[1].y, 0f));
		Debug.DrawLine(new Vector3(bounds[1].x, bounds[1].y, 0f), new Vector3(bounds[3].x, bounds[3].y, 0f));
		Debug.DrawLine(new Vector3(bounds[3].x, bounds[3].y, 0f), new Vector3(bounds[2].x, bounds[2].y, 0f));
		Debug.DrawLine(new Vector3(bounds[2].x, bounds[2].y, 0f), new Vector3(bounds[0].x, bounds[0].y, 0f));





	}


	public void Squish()
	{
		rbd.AddForce(Vector3.up * pushForce, ForceMode2D.Impulse);
		Debug.Log("squish");
	}

	bool CheckBounds(Vector3 worldPos)
	{
		
		if (worldPos.x >= bounds[0].x  && worldPos.y <= bounds[0].y &&
			worldPos.x <= bounds[1].x && worldPos.y <= bounds[1].y &&
			worldPos.y >= bounds[3].y)
			//worldPos.x >= bounds[2].x && worldPos.y >= bounds[2].y && 
			//worldPos.x <= bounds[3].x && worldPos.y >= bounds[3].y)
	
			return true;
		return false;
	}

	void OnMouseDrag()
	{
		
		Vector3 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		if (CheckBounds(worldPos))
		{
			worldPos.z = rbd.transform.position.z;
			rbd.transform.position = worldPos;
			prevMousePosition =  Camera.main.ScreenToWorldPoint(Input.mousePosition);
		}

	}



	void OnMouseUp()
	{
		
		Vector3 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		if (CheckBounds(worldPos))
		{
			rbd.velocity = ((worldPos - prevMousePosition) * pushForce);
		}

	}

	/*IEnumerator OnCollisionEnter2D(Collision2D col)
	{
		outOfScreen = true;
		rbd.velocity = Vector3.zero;


	}*/




}
