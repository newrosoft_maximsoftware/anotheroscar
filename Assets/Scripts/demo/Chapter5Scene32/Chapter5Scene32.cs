﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter5Scene32 : MonoBehaviour {
	private enum State{None,Paused,Resumed};
	private State currentState=State.None;
	private bool wasClicked = false;

	Animator anim;

	void Start()
	{
		anim=GetComponent<Animator> ();
	}

	public void ToggleClicked()
	{
		wasClicked = !wasClicked;
		anim.SetTrigger ("wasClicked");
	}

	public void OnDisable()
	{
		Resume();
	}

	public void Pause()
	{
		if (currentState == State.None) {
			anim.enabled = false;
			currentState = State.Paused;
		}
	}

	public void Resume()
	{
		anim.enabled = true;
		currentState = State.Resumed;
		ToggleClicked ();
	}

}
