﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationReset : MonoBehaviour {
	private bool t;
	private Animator anim;
	void Start () 
	{
		anim = GetComponent<Animator>();
	}
	

	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.G))
		{
			t = !t;
			int hash = Animator.StringToHash("Default");

			anim.Play(hash, -1, 1);
			anim.speed = t?1f:0f;
		}
	}




}
