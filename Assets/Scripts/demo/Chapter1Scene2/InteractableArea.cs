﻿using UnityEngine;
using System.Collections;

public class InteractableArea : MonoBehaviour {
	public GameObject hugosArm;

	public GameObject light;
	private bool lightOn = true;


	private Quaternion place;


	// Use this for initialization
	void Start () 
	{
		place = hugosArm.transform.rotation;
		ToggleLight();



	}


	

	void Update () 
	{

	}

	void OnMouseDrag()
	{
		if (!lightOn) 
		{
			ToggleLight();
			ScenarioManager.instance.step++;
		}

		Vector3 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		worldPos.z = hugosArm.transform.position.z;

		//if ((hugosArm.transform.rotation.eulerAngles.x <= 314f) && (hugosArm.transform.rotation.eulerAngles.x>=50))
		{

			hugosArm.transform.LookAt(worldPos);
		}

		//Debug.Log(hugosArm.transform.rotation.eulerAngles);

	}

	void OnMouseUp()
	{
		StartCoroutine(RotateHandTo(place));

	}

	void ToggleLight()
	{
		lightOn = !lightOn;
		light.SetActive(lightOn);
	}

	IEnumerator RotateHandTo(Quaternion q)
	{

		float startTime = Time.time;
		while (hugosArm.transform.rotation != place)
		{
			hugosArm.transform.rotation = Quaternion.Lerp(hugosArm.transform.rotation, q, Time.time - startTime);
			yield return null;
		}


	}
}
