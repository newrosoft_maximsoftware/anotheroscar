﻿using UnityEngine;
using System.Collections;

public class LightOrbs : MonoBehaviour {
	private EdgeCollider2D col;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator OnTriggerEnter2D(Collider2D other)
	{
		float r = Random.Range(0.1f, 1.7f);
		yield return new WaitForSeconds(r);
		other.gameObject.GetComponent<Animator>().SetBool("isPulsing", true);
	}

	void OnTriggerExit2D(Collider2D other)
	{
		other.gameObject.GetComponent<Animator>().SetBool("isPulsing", false);
	}
}
