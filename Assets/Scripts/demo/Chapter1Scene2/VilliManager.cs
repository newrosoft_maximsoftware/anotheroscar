﻿using UnityEngine;
using System.Collections;

public class VilliManager : MonoBehaviour {
	public Animator[] eyelids;
	// Use this for initialization
	void Start () {
		InvokeRepeating("Randomness", 0f, 2f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Randomness()
	{
		for (int i=0; i<eyelids.Length; i++)
		{
			int r = Random.Range(0, 3);
			if (r == 1)
				eyelids[i].SetTrigger("blink");
		}
	}


}
