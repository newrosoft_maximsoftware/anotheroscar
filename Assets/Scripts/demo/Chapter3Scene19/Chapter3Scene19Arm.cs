﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Chapter3Scene19Arm : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	private bool isGlowing=true;
	Animator anim;
	private float timePassed = 0.0f;
	private float maxTime=3f;
	public Vector2 rotationLimit;
	public float rotationSpeed=1000f;
	private Vector2 clickedPosition;

	void Start () {
		anim = GetComponent<Animator> ();
	}

	void OnEnable()
	{
		if (!isGlowing)
			ToggleGlowing ();

		transform.rotation=Quaternion.identity;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		ToggleGlowing ();
		clickedPosition = eventData.position;

	}

	public void OnDrag(PointerEventData eventData)
	{

		Vector3 origin=clickedPosition;
		Vector3 newPosition =eventData.position;

		Vector3 relativePosition = origin - newPosition;
		float direction = -1f;

		Quaternion rotationSimultaion = transform.rotation;
		rotationSimultaion.eulerAngles += Vector3.forward * relativePosition.y*direction*Time.deltaTime*9f;

        
		if ((rotationSimultaion.eulerAngles.z <= rotationLimit.x & rotationSimultaion.eulerAngles.z >= 0) ||
			(rotationSimultaion.eulerAngles.z >= rotationLimit.y & rotationSimultaion.eulerAngles.z <=360))
			transform.rotation = rotationSimultaion;

		clickedPosition = eventData.position;

	}
		
	public void OnEndDrag(PointerEventData eventData)
	{
		ToggleGlowing ();
	}

	public void ToggleGlowing()
	{
		isGlowing = !isGlowing;
		anim.SetBool ("isGlowing",isGlowing);
	}
}
