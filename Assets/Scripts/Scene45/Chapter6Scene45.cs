﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Chapter6Scene45 : SceneScript,IDragHandler,IBeginDragHandler,IEndDragHandler,IPointerUpHandler,IPointerExitHandler,IPointerEnterHandler,IPointerDownHandler {
	enum State{None,Stationary,Increase,Decrease};
	private int hashIsGlowing=Animator.StringToHash("isGlowing");
	private int hashIsRubbing=Animator.StringToHash("isRubbing");
	private int hashContinueAnimation=Animator.StringToHash("triggerContinue");

	private float increasedSize=22.8f;
	float imageHeight=228f;
	public Image filledImage;
	public Image stick;
	private float secondsToFill=10.0f;
	private bool adding=false;
	Coroutine countdownAdd;
	Coroutine countdownDecrease;
	Vector2 lastPosition;

	bool isDown=false;
	private State state;

	void Start()
	{
		base.Start ();
		StartCoroutine (Main ());
	}

	IEnumerator Main()
	{
		while (true) {
			if (state == State.Increase) {
				if (!anim.GetBool (hashIsRubbing)) {
					SetIsRubbing (true);
					SetIsGlowing (false);
				}
				Increase ();
			} else if (state == State.Decrease) {
				if (anim.GetBool (hashIsRubbing)) {
					SetIsRubbing (false);
					SetIsGlowing (true);
				}
				Decrease ();
			} else if (state == State.Stationary) {
				if(anim.GetBool(hashIsRubbing))
					SetIsRubbing(false);
				Decrease ();
			}
			yield return null;
		}
	}

	void ContinueAnimation()
	{
		anim.SetBool (hashContinueAnimation);
	}

	public void SetIsGlowing(int isGlowing)
	{
		bool glowing = false;

		if (isGlowing == 1)
			glowing = true;

		anim.SetBool (hashIsGlowing,glowing);
	}

	public void SetIsGlowing(bool isGlowing)
	{
		anim.SetBool (hashIsGlowing,isGlowing);
	}

	public void SetIsRubbing(bool isRubbing)
	{
		if (isRubbing == true)
			stick.enabled = true;
		else
			stick.enabled=false;
		
		anim.SetBool (hashIsRubbing,isRubbing);
	}

	IEnumerator DecreaseCo()
	{
		while (true) {
			Decrease ();
			yield return null;
		}
	}

	public void Increase()
	{
		

		Vector2 sizeDelta= filledImage.rectTransform.sizeDelta;
		if (sizeDelta.y < imageHeight) {
			sizeDelta.y += increasedSize * Time.deltaTime;
			filledImage.rectTransform.sizeDelta = sizeDelta;
		} else {
			ContinueAnimation ();
		}
	}

	public void Decrease()
	{
		
		
		Vector2 sizeDelta= filledImage.rectTransform.sizeDelta;
		if (sizeDelta.y > 0) {
			sizeDelta.y -= increasedSize * Time.deltaTime;
			filledImage.rectTransform.sizeDelta = sizeDelta;
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		Debug.LogError ("enter pointer");
		/*Debug.LogError ("enter pointer");
		lastPosition = eventData.position;
		SetIsGlowing (false);
		if (countdownDecrease != null)
		StopCoroutine (countdownDecrease);*/
		lastPosition = eventData.position;
		//isDown = true;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		Debug.LogError ("begin drag");
		isDown = true;
		lastPosition = eventData.position;
		/*Debug.LogError ("begin drag");

		SetIsGlowing (false);
		if (countdownDecrease != null)
			StopCoroutine (countdownDecrease);*/
	}

	public void OnDrag(PointerEventData eventData)
	{
		/*Debug.LogError (" drag");
		
		if (lastPosition != eventData.position) {
			lastPosition = eventData.position;
			Increase ();
		} else {
			Debug.LogError ("holdPosition");
			Decrease ();
		}*/
		Debug.LogError (lastPosition + "    " + eventData.position);
		float distance = (lastPosition - eventData.position).magnitude;

		Debug.LogError ("drag");
		if (isDown)
			state = State.Increase;
		lastPosition = eventData.position;
	}
				
	public void OnEndDrag(PointerEventData eventData)
	{
	/*	Debug.LogError ("end drag");
		countdownDecrease = StartCoroutine (DecreaseCo ());
		SetIsGlowing (true);*/
		Debug.LogError ("end drag");

		isDown = false;
		state = State.Decrease;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		/*Debug.LogError ("pointer up");
			countdownDecrease = StartCoroutine (DecreaseCo ());
		SetIsGlowing (true);*/
		state = State.Decrease;
		isDown = false;
		Debug.LogError ("pointer up");
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		/*Debug.LogError ("pointer up");
			countdownDecrease = StartCoroutine (DecreaseCo ());
		SetIsGlowing (true);*/
		lastPosition = eventData.position;
		Debug.LogError ("pointer down");
		//state = State.Increase;
		isDown = true;
	//	StartCoroutine (Wait ());
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		Debug.LogError ("exit pointer");
		isDown = false;
		/*Debug.LogError ("exit pointer");
			countdownDecrease = StartCoroutine (DecreaseCo ());
		SetIsGlowing (true);*/
		state = State.Decrease;
	}

	IEnumerator Wait()
	{
		yield return new WaitForEndOfFrame ();
		if (Input.touchCount > 0) {
			if(Input.GetTouch(0).position==lastPosition)
				state = State.Stationary;
		}
	}
}
