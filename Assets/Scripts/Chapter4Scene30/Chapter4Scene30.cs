﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Chapter4Scene30 : MonoBehaviour {

	private  bool [] isGlowing;
	private  bool [] canBePressed;
	Animator mainAnimator;
	List<Animator> animators;
	void Awake () {
		mainAnimator = GetComponent<Animator> ();
		Animator []animTemp = GetComponentsInChildren<Animator> ();
		animators = new List<Animator> ();
		for (int i = 1; i < animTemp.Length; i++)
		{
			animators.Add (animTemp[i]);
		}

		isGlowing = new bool[animators.Count];
		canBePressed = new bool[animators.Count];

	}

	void OnEnable()
	{
		ToggleButterflyPile (-1);
		for (int i = 0; i < animators.Count; i++) {			
			isGlowing [i] = false;
			canBePressed [i] = false;
			animators[i].SetBool ("wasClicked",canBePressed[i]);
			animators[i].SetBool ("isGlowing",isGlowing[i]);
		}

	}
	
	public void ToggleGlowing(int butterflyId)
	{
		isGlowing[butterflyId] = !isGlowing[butterflyId];

		if(isGlowing[butterflyId])			
			canBePressed[butterflyId] = true;
		else 
			canBePressed[butterflyId] = false;
		
		animators[butterflyId].SetBool ("isGlowing",isGlowing[butterflyId]);

	}

	public void WasClicked(int butterflyId)
	{
		if (canBePressed[butterflyId]) {
			animators[butterflyId].SetBool ("wasClicked", canBePressed[butterflyId]);
			canBePressed[butterflyId] = !canBePressed[butterflyId];
			ToggleGlowing (butterflyId);
			ToggleButterflyPile (butterflyId);
		}
	}

	public void ToggleButterflyPile(int butterflyId)
	{
		mainAnimator.SetInteger ("butterflyId",butterflyId);
	}


}
