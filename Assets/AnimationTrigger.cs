﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationTrigger: MonoBehaviour {

	[Header("Scene9")]
	public UnityEvent doorClickable;

	Animator anim;

	void Start()
	{
		anim=GetComponent<Animator>();
	}

	public void animate()
	{
		doorClickable.Invoke();
	}


}
