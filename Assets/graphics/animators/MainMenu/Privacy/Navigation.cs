﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navigation : MonoBehaviour {

	public void Navigate(string url)
	{
		Application.OpenURL (url);
	}
}
