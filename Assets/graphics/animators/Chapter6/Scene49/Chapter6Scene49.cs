﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chapter6Scene49 : SceneScript {
    
    public GameObject door;
    public GameObject doorway;

    public void ChangeOrder()
    {
        int poz=doorway.transform.GetSiblingIndex();
        door.transform.SetSiblingIndex(poz);
    }

}
