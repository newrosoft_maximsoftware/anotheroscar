﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chapter5Scene42SliderScript : MonoBehaviour {

	public GameObject leftSpace;
	public GameObject rightSpace;
	public GameObject parent;
	public Image []  images;
	int currentImage=0;
	int hashSlide=Animator.StringToHash("slide");
	int hashGlowing=Animator.StringToHash("isGlowing");
	int hashStarted=Animator.StringToHash("started");
	Animator anim;
	bool isGlowing=false;
	int [] positions;

	void Start () {
		anim=GetComponent<Animator>();
		positions=new int[images.Length];
		InitPositions();
	}

	void InitPositions()
	{
		for(int i=0;i<positions.Length;i++)
			positions[i]=i;
	}

	void ResetImages()
	{
		int n=images.Length;

		images[0].enabled=true;
		for(int i=1;i<n;i++)
			images[i].enabled=false;
	}

	public void SetIsGlowing(bool isGlowing)
	{
		this.isGlowing=isGlowing;
		anim.SetBool(hashGlowing,isGlowing);
	}

	public void Started(bool started)
	{
		anim.SetBool(hashStarted,started);
	}
	
	public void Slide()
	{
		if(isGlowing)
		{
		SetIsGlowing(false);
		if(currentImage==images.Length-1)
		{
			currentImage=0;
				int aux=positions[positions.Length-1];
				for(int i=positions.Length-1;i>0;i--)
			{
					positions[i]=positions[i-1];
			}
				positions[0]=aux;
		}
		
			SetupImages(images[positions[currentImage]],images[positions[currentImage+1]]);
		anim.SetTrigger(hashSlide);
		}
	}

	void OnDisable()
	{
		Reset();
	}

	void Reset()
	{
		InitPositions();
		ResetImages();
		currentImage=0;
		SetIsGlowing(false);
		Started(false);
	}

	public void TransitionEnd()
	{
		RestoreImages(images[positions[currentImage]],images[positions[currentImage+1]]);
		currentImage++;
		SetIsGlowing(true);
	}

	public void StartEnded()
	{
		SetIsGlowing(true);
	}

	public void SetupImages(Image leftImage,Image rightImage)
	{
		Debug.LogError(leftImage.name+"   "+rightImage.name);
		for(int i=0;i<images.Length;i++)
			Debug.LogError(images[i].name);
		leftImage.enabled=true;
		rightImage.enabled=true;

		leftImage.transform.parent=leftSpace.transform;
		leftImage.transform.localPosition=Vector3.zero;

		rightImage.transform.parent=rightSpace.transform;
		rightImage.transform.localPosition=Vector3.zero;
	}

	public void RestoreImages(Image leftImage,Image rightImage)
	{
		leftImage.transform.parent=parent.transform;
		leftImage.transform.localPosition=Vector3.zero;

		rightImage.transform.parent=parent.transform;
		rightImage.transform.localPosition=Vector3.zero;

		leftImage.enabled=false;
	}
}
