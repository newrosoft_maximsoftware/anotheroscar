﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ch3Sc17MrPots : MonoBehaviour {

	private bool isGlowing=true;
	private bool isClickable=true;
	private int speechNr=0;
	Animator anim;
	private float timePassed = 0.0f;
	private float maxTime=3f;
	private int speechCount=3;
	void Start () {
		anim = GetComponent<Animator> ();
	}

	public void ToggleGlowing()
	{
		isGlowing = !isGlowing;
		anim.SetBool ("isGlowing",isGlowing);
	}

	public void NextSpeech ()
	{
		speechNr++;
		anim.SetInteger ("speech",speechNr);
	}

	public void ClickedPots()
	{
		if (isClickable) {
			isClickable = !isClickable;
			ToggleGlowing ();
			NextSpeech ();
			StartCoroutine (ClickableCountdown());
		}
	}

	private IEnumerator ClickableCountdown()
	{
		if (speechNr < speechCount) {
			while (timePassed < maxTime) {
				timePassed += Time.deltaTime;
				yield return null;
			}

			timePassed = 0f;
			isClickable = true;
			ToggleGlowing ();
		}
	}
}
