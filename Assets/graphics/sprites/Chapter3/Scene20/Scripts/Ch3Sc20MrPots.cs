﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ch3Sc20MrPots : MonoBehaviour {

	private bool isGlowing=false;
	private bool isClickable=false;
	private int speechNr=0;
	Animator anim;
	private float timePassed = 0.0f;
	private float maxTime=3f;
	private int speechCount=2;

	void Start () {
		
	}

	public void Awake()
	{
		anim = GetComponent<Animator> ();
	}

	public void OnEnable()
	{
		isGlowing = false;
		isClickable = false;
		speechNr = 0;
		timePassed = 0.0f;
		anim.SetBool ("isGlowing",isGlowing);
		anim.SetInteger ("speech",speechNr);
	}

	public void Animate()
	{
		ToggleGlowing ();
		isClickable = true;
	}

	public void ToggleGlowing()
	{
		isGlowing = !isGlowing;
		anim.SetBool ("isGlowing",isGlowing);
	}

	public void NextSpeech ()
	{
		speechNr++;

		if (speechNr > speechCount)
			speechNr = 1;
		
		anim.SetInteger ("speech",speechNr);
	}

	public void ClickedPots()
	{
		if (isClickable) {
			isClickable = !isClickable;
			ToggleGlowing ();
			NextSpeech ();
			StartCoroutine (ClickableCountdown());
		}
	}

	private IEnumerator ClickableCountdown()
	{
		if (speechNr <= speechCount) {
			while (timePassed < maxTime) {
				timePassed += Time.deltaTime;
				yield return null;
			}

			timePassed = 0f;
			isClickable = true;
			ToggleGlowing ();
		}
	}
}
