﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionPanel : MonoBehaviour {

	public Text question;
	public Text [] answers;

	public void SetQuestion(string text)
	{
		question.text = text;
	}

	public void SetAnswer(int nr,string text)
	{
		answers [nr].text = text;
	}
}
