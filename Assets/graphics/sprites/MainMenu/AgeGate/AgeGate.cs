﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.UI;

public class AgeGate : MonoBehaviour {

	public GameObject questionPanel;
	public GameObject correctAnswerPanel;
	public GameObject wrongAnswerPanel;

	private QuestionPanel questionPanelScript;
	private int selectedQuestion=0;

	List<Question> questions = new List<Question> ();
	string path="xml/AgeGateQuestions";
	bool ageChecked=false;
	string url;

	void Start () {
		questionPanelScript = questionPanel.GetComponent<QuestionPanel> ();
		LoadQuestions ();
	}

	void LoadQuestions()
	{
		TextAsset textAsset = (TextAsset) Resources.Load(path);  
		XmlDocument xmldoc = new XmlDocument ();
		xmldoc.LoadXml ( textAsset.text );

		XmlNodeList xmlQuestions = xmldoc.SelectNodes ("AgeGate/Question");

		foreach (XmlNode node in xmlQuestions) {
			Question question = new Question ();
			question.content = node ["Content"].InnerXml;

			XmlNodeList xmlAnswers = node.SelectNodes ("Answer");

			foreach(XmlNode xmlAnswer in xmlAnswers)
			{
				Answer answer = new Answer ();
				answer.content = xmlAnswer ["Content"].InnerText;
				answer.correctAnswer = bool.Parse (xmlAnswer ["CorrectAnswer"].InnerText);
				question.answers.Add (answer);
			}

			questions.Add (question);
		}
	}

	public void ShowQuestionPanel()
	{
		questionPanel.SetActive (true);
		correctAnswerPanel.SetActive (false);
		wrongAnswerPanel.SetActive (false);
		questionPanel.transform.parent.SetAsLastSibling ();
		FillTheQuestion ();
	}

	public void OpenWebPage(string url)
	{
		this.url = url;

		if (ageChecked)
			Navigate (url);
		else {
			ShowQuestionPanel ();
		}
	}

	public void Navigate(string url)
	{
		Application.OpenURL (url);
		ageChecked = correctAnswerPanel.GetComponentInChildren<Toggle> ().isOn;
		HideAll ();
	}

	public void Navigate()
	{
		ageChecked = correctAnswerPanel.GetComponentInChildren<Toggle> ().isOn;
		HideAll ();
		Application.OpenURL (url);
	}

	void FillTheQuestion()
	{
		selectedQuestion = Random.Range (0,questions.Count);
		Question question = questions [selectedQuestion];
		questionPanelScript.SetQuestion (question.content);
		Answer [] randomizedAnswers=new Answer[question.answers.Count];

		for (int i = 0; i < question.answers.Count; i++) {
			int poz;
			do {
				poz = Random.Range (0, question.answers.Count);

			} while(randomizedAnswers [poz] != null);
			if (randomizedAnswers [poz] == null)
				randomizedAnswers [poz] = question.answers [i];
		}

		List<Answer> newAnswers = new List<Answer> (randomizedAnswers);
		question.answers = newAnswers;

		for (int i = 0; i < question.answers.Count; i++)
			questionPanelScript.SetAnswer (i, question.answers [i].content);
	}

	public void AnswerClicked(int index)
	{
		if (questions [selectedQuestion].answers [index].correctAnswer) {
			ShowCorrectAnswerPanel ();
		} else {
			ShowWrongAnswerPanel ();
		}
	}

	public void ShowCorrectAnswerPanel()
		{
		questionPanel.SetActive (false);
		correctAnswerPanel.SetActive (true);
		wrongAnswerPanel.SetActive (false);
		correctAnswerPanel.transform.parent.SetAsLastSibling ();
		}

		public void ShowWrongAnswerPanel()
		{
		questionPanel.SetActive (false);
		correctAnswerPanel.SetActive (false);
		wrongAnswerPanel.SetActive (true);
		wrongAnswerPanel.transform.parent.SetAsLastSibling ();
		}

	public void HideAll()
	{
		questionPanel.SetActive (false);
		correctAnswerPanel.SetActive (false);
		wrongAnswerPanel.SetActive (false);
	}
}
