﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardSlider : MonoBehaviour {
	public GameObject[] cards;
	public Button[] dots;
	public Sprite[] dotSprites;
	Dictionary<int,GameObject> cardDotConnection;
	int currentCard=0;
	HorizontalLayoutGroup group;
	void Start () {
		InitCards ();
		group = cards [0].transform.parent.gameObject.GetComponent<HorizontalLayoutGroup> ();
	}

	void InitCards()
	{
		cardDotConnection = new Dictionary<int, GameObject> ();

		for (int i = 0; i < cards.Length; i++)
			cardDotConnection.Add (i,cards[i]);
	}

	public void ChangeCard(int poz)
	{
		if (poz != currentCard) {
			currentCard = poz;
			ActivateDot (poz);
			MoveCard (poz);
		}
	}

	public void MoveCard(int poz)
	{
		for (int i = 0; i < cards.Length; i++) {
			cards [i] = cardDotConnection [i];
			cards [i].transform.SetSiblingIndex (i);
		}


		GameObject card = cards [0];
		cards [0] = cards [poz];
		cards [poz] = card;

		cards [0].transform.SetSiblingIndex (0);

	}

	public void ActivateDot(int poz)
	{
		for (int i = 0; i < dots.Length; i++)
			if (i == poz)
				dots [i].image.sprite = dotSprites [1];
			else
				dots [i].image.sprite = dotSprites [0];
				
	}
}
